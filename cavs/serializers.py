from rest_framework import serializers

from .models import Movie, Review, Rating, Actor

class FilterReviewListSerializer(serializers.ListSerializer):
    """ Only parents """

    def to_representation(self, data):
        data = data.filter(parent=None)
        return super().to_representation(data)

class RecursiveSerializer(serializers.Serializer):
    """ Recursive list children """

    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data

class ReviewCreateSrializer(serializers.ModelSerializer):
    """ Create review """

    class Meta:
        model = Review
        fields = "__all__"

class ActorListSerializer(serializers.ModelSerializer):
    """ Actor list """

    class Meta:
        model = Actor
        fields = ("id", "name", "image")

class ActorDetailSerializer(serializers.ModelSerializer):
    """ Full information about actor"""

    class Meta:
        model = Actor
        fields = "__all__"

class MovieListSerializer(serializers.ModelSerializer):
    """ Film list """
    ratings_user = serializers.BooleanField()
    middle_star = serializers.IntegerField()

    class Meta:
        model = Movie
        fields = ("id", "title", "tagline", "category", "ratings_user", "middle_star")

class ReviewSrializer(serializers.ModelSerializer):
    """ Review list """

    children = RecursiveSerializer(many=True)

    class Meta:
        list_serializer_class = FilterReviewListSerializer
        model = Review
        fields = ("name", "text", "children")

class MovieDetailSerializer(serializers.ModelSerializer):
    """ FIlm detail """

    category = serializers.SlugRelatedField(slug_field="name", read_only=True)
    directors = ActorListSerializer(read_only=True, many=True)
    actors = ActorListSerializer(read_only=True, many=True)
    genres = serializers.SlugRelatedField(slug_field="name", read_only=True, many=True)
    reviews = ReviewSrializer(many=True)

    class Meta:
        model = Movie
        exclude = ("draft", )

class CreateRaitingSerializer(serializers.ModelSerializer):
    """ Add raiting """

    class Meta:
        model = Rating 
        fields = ("movie", "star")
    
    def create(self, validated_data):
        raiting, _ = Rating.objects.update_or_create(
            ip=validated_data.get('ip', None),
            movie=validated_data.get('movie', None),
            defaults={'star': validated_data.get('star')}
        )
        
        return raiting