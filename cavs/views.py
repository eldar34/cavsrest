from django.db import models
from rest_framework import generics, permissions, viewsets
from django_filters.rest_framework import DjangoFilterBackend 
from rest_framework.decorators import action

# Create your views here.

from .models import Movie, Actor
from .serializers import (
    MovieListSerializer, 
    MovieDetailSerializer, 
    ReviewCreateSrializer, 
    CreateRaitingSerializer,
    ActorListSerializer,
    ActorDetailSerializer,
)
from .service import get_client_ip, MovieFIlter

class MovieViewSet(viewsets.ReadOnlyModelViewSet):
    """ Film list """

    filter_backends = (DjangoFilterBackend, )
    filterset_class = MovieFIlter
    # permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        movies = Movie.objects.filter(draft=False).annotate(
            ratings_user=models.Count("ratings", filter=models.Q(ratings__ip=get_client_ip(self.request)))
        ).annotate(
            middle_star=models.Sum(models.F('ratings__star')) / models.Count(models.F('ratings'))
        )
        return movies
    
    def get_serializer_class(self):
        if self.action == 'list':
            return MovieListSerializer
        elif self.action == 'retrieve':
            return MovieDetailSerializer

class ReviewCreateViewSet(viewsets.ModelViewSet):
    serializer_class = ReviewCreateSrializer

class AddStarRatingViewSet(viewsets.ModelViewSet):
    serializer_class = CreateRaitingSerializer

    def perform_create(self, serializer):
        serializer.save(ip=get_client_ip(self.request))

class ActorViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Actor.objects.all()

    def get_serializer_class(self):
        if self.action == 'list':
            return ActorListSerializer
        elif self.action == 'retrieve':
            return ActorDetailSerializer
        

# class MovieDetailView(generics.RetrieveAPIView):
#     """ Film detail """
    
#     queryset = Movie.objects.filter(draft=False)
#     serializer_class = MovieDetailSerializer

        

# class ReviewCreateView(generics.CreateAPIView):
#     """ Create review """

#     serializer_class = ReviewCreateSrializer

# class AddRaitingView(generics.CreateAPIView):
#     """ Add raiting to film """

#     serializer_class = CreateRaitingSerializer
    
#     def perform_create(self, serializer):    
#         serializer.save(ip=get_client_ip(self.request))

# class ActorListView(generics.ListAPIView):
#     """ Generic ListView actors """

#     queryset = Actor.objects.all()
#     serializer_class = ActorListSerializer

# class ActorDetailView(generics.RetrieveAPIView):
#     """ Generic DetailView actors """

#     queryset = Actor.objects.all()
#     serializer_class = ActorDetailSerializer





